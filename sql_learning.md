http://sqlzoo.net/wiki/SQL_Tutorial

# SELECT basics.

    1.1. SELECT population FROM world WHERE name='Germany';
    1.2. SELECT name,population FROM world WHERE name IN ('Sweden', 'Norway', 'Denmark');
    1.3. SELECT name, area FROM world  WHERE area BETWEEN 200000 AND 250000;
# SELECT from WORLD Tutorial
    2.1. SELECT name, continent, population FROM world;
    2.2. SELECT name FROM world WHERE population >= 200000000;
    2.3. SELECT name, gdp/population as 'per capital GDP' FROM world WHERE population>=200000000;
    2.4. SELECT name, population / 1000000 FROM world WHERE continent='South America';
    2.5. SELECT name, population FROM world WHERE name in ('France', 'Germany', 'Italy');
    2.6. SELECT name FROM world WHERE name LIKE '%United%';
    2.7. SELECT name, population, area FROM world WHERE area>=3000000 OR population>=250000000;
    2.8. SELECT name, population, area FROM world WHERE area>=3000000 XOR population>=250000000;
    2.9. SELECT name, ROUND(population/1000000,2) AS population, ROUND(gdp/1000000000,2) AS GDP FROM world WHERE continent='South America'; 
    2.10. SELECT name, ROUND(gdp/population,-3) AS 'per capital GDP' FROM world WHERE gdp>1000000000000 ORDER BY gdp;
    2.11. SELECT name, capital FROM world WHERE LENGTH(name)=LENGTH(capital);
    2.12. SELECT name, capital FROM world WHERE  LEFT(name,1)=LEFT(capital,1) AND name <> capital;
    2.13. SELECT name   FROM world WHERE name LIKE '%a%'AND name LIKE '%e%'AND name LIKE '%i%'AND name LIKE '%o%'AND name LIKE '%u%'
  AND name NOT LIKE '% %';
 
#  SELECT from Nobel Tutorial
    3.1. SELECT yr, subject, winner FROM nobel WHERE yr = 1950;
    3.2. SELECT winner  FROM nobel WHERE yr = 1962   AND subject = 'Literature';
    3.3. SELECT yr, subject  FROM nobel WHERE winner='Albert Einstein';
    3.4. SELECT winner  FROM nobel WHERE subject='Peace' AND yr>=2000;
    3.5. SELECT yr, subject, winner  FROM nobel WHERE subject='Literature' AND yr BETWEEN 1980 AND 1989;
    3.6. SELECT * FROM nobel WHERE winner IN ('Theodore Roosevelt','Woodrow Wilson','Jimmy Carter', 'Barack Obama');
    3.7. SELECT winner FROM nobel WHERE winner LIKE 'John%';
    3.8. SELECT yr, subject, winner FROM nobel WHERE (subject='Physics' AND yr=1980) OR (subject='Chemistry' AND yr=1984) ; 
    3.9. SELECT yr, subject, winner FROM nobel WHERE yr=1980 AND subject NOT IN ('Chemistry','MEDICINE');
    3.10. SELECT yr, subject, winner FROM nobel WHERE (yr<1910 AND subject='medicine') OR(yr>=2004 AND subject='Literature');
    3.11. SELECT yr, subject, winner FROM nobel WHERE winner='Peter Grünberg';
    3.12. SELECT yr, subject, winner FROM nobel WHERE winner='EUGENE O''NEILL ';
    3.13. SELECT winner, yr, subject FROM nobel WHERE winner LIKE 'Sir%' ORDER BY yr DESC, winner;
    3.14. SELECT winner, subject FROM nobel WHERE yr=1984 ORDER BY subject IN ('Physics','Chemistry'),subject, winner;

# SELECT within SELECT Tutorial
    4.1. SELECT within SELECT Tutorial;
    4.2. SELECT name FROM world  WHERE gdp/population > (SELECT gdp/population FROM world WHERE name='United Kingdom')  AND continent='Europe';
    4.3. SELECT name, continent FROM world  WHERE continent IN (SELECT continent FROM world WHERE name='Argentina' OR name='Australia') ORDER BY name;
    4.4. SELECT name, population FROM world  WHERE population > (SELECT  population FROM world WHERE name='Canada') AND population < (SELECT  population FROM world WHERE name='Poland') ;
    4.5. SELECT name, CONCAT(ROUND(population/(SELECT population FROM world WHERE name='Germany')*100), '%')  AS percentage_over_Germany FROM world  WHERE continent='Europe';
    4.6. SELECT name FROM world  WHERE gdp>(SELECT max(gdp) FROM world WHERE continent='Europe');
    4.7. SELECT continent, name, area FROM world x WHERE area=(SELECT MAX(area) FROM world y WHERE y.continent=x.continent );
    4.8. SELECT continent, name FROM world x WHERE x.name=(SELECT name FROM world y WHERE x.continent=y.continent LIMIT 1) ;
    4.9. SELECT name, continent, population FROM world x WHERE x.continent IN(SELECT continent FROM (SELECT MAX(population) as max_pop, continent FROM world GROUP BY continent) y WHERE y.max_pop<=25000000);
    4.10. SELECT name, continent FROM world x WHERE x.population> ALL(SELECT 3*population FROM world y  WHERE y.continent=x.continent and y.population <> population);
        
        
# SUM and COUNT

    5.1. SELECT SUM(population) FROM world;
    5.2. SELECT continent FROM world GROUP BY continent;
    5.3. SELECT sum(gdp) FROM world WHERE continent='Africa';
    5.4. SELECT COUNT(*) FROM world WHERE area>=1000000;
    5.5. SELECT SUM(population) FROM world WHERE name IN ('Estonia', 'Latvia', 'Lithuania');
    5.6. SELECT continent, COUNT(*) FROM world GROUP BY continent;
    5.7. SELECT continent, COUNT(*) FROM world WHERE population >=10000000 GROUP BY continent;
    5.8. SELECT continent FROM world GROUP BY continent HAVING(SUM(population)>=100000000);
    
# More JOIN operations

    7.1. SELECT id, title FROM movie WHERE yr=1962;
    7.2. SELECT yr FROM movie WHERE title= 'Citizen Kane';
    7.3. SELECT id, title, yr FROM movie WHERE title LIKE '%Star Trek%' ORDER BY yr;
    7.4. SELECT id FROM actor WHERE name= 'Glenn Close';
    7.5. SELECT id FROM movie WHERE title='Casablanca';
    7.6. SELECT name FROM actor INNER JOIN casting ON casting.actorid=actor.id WHERE casting.movieid=11768;
    7.7. SELECT name FROM actor INNER JOIN casting ON casting.actorid=actor.id WHERE casting.movieid=(SELECT id FROM movie WHERE title='Alien');
    7.8. SELECT title FROM movie INNER JOIN casting ON casting.movieid=movie.id WHERE casting.actorid=(SELECT id FROM actor WHERE name='Harrison Ford');
    7.9. SELECT title FROM movie INNER JOIN casting ON casting.movieid=movie.id WHERE casting.actorid=(SELECT id FROM actor WHERE name='Harrison Ford') AND casting.ord <> 1;
    7.10. SELECT movie.title, actor.name FROM movie INNER JOIN casting ON casting.movieid= movie.id INNER JOIN actor ON actor.id=casting.actorid  WHERE movie.yr=1962 AND casting.ord=1;
    7.11. SELECT yr,COUNT(title) FROM   movie JOIN casting ON ovie.id=movieid
         JOIN actor   ON actorid=actor.id WHERE name='John Travolta' GROUP BY yr HAVING COUNT(title)=(SELECT MAX(c) FROM (SELECT yr,COUNT(title) AS c FROM    movie JOIN casting ON movie.id=movieid JOIN actor ON actorid=actor.id where name='John Travolta' GROUP BY yr) AS t);
    7.12. SELECT movie.title, actor.name FROM movie JOIN casting ON casting.movieid=movie.id JOIN actor ON casting.actorid=actor.id WHERE movie.id IN (SELECT movieid FROM casting WHERE actorid=(SELECT id FROM actor WHERE name='Julie Andrews')) AND casting.ord=1; 
    7.13.SELECT name FROM (SELECT actor.name, COUNT(*) as x FROM actor JOIN casting ON casting.actorid=actor.id WHERE casting.ord=1 GROUP BY actor.name) a WHERE a.x>=30 ORDER BY name;
    7.14. SELECT movie.title, COUNT(movie.title) AS c FROM movie JOIN casting ON casting.movieid=movie.id WHERE movie.yr=1978 GROUP BY movie.title ORDER BY c DESC, movie.title;
    7.15. SELECT actor.name FROM actor JOIN casting ON casting.actorid=actor.id WHERE casting.movieid IN (SELECT movieid FROM casting WHERE casting.actorid=(SELECT actor.id FROM actor WHERE actor.name='Art Garfunkel')) AND actor.name <> 'Art Garfunkel' GROUP BY actor.name;
